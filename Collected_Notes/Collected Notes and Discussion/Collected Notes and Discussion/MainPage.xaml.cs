﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Collected_Notes_and_Discussion.ViewModel;
using Collected_Notes_and_Discussion.Resources;
using Collected_Notes_and_Discussion.Model;
using System.IO;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http;

namespace Collected_Notes_and_Discussion
{
    public partial class MainPage : PhoneApplicationPage
    {
        
        // Constructor
        MainViewModel MyVM = new MainViewModel();
        public MainPage()
        {
            InitializeComponent();
            //MyVM.LoadData();
            
            DataContext = MyVM;
            
             
            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar(); 
            Loaded += MainPage_Loaded;
        }

        async void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            //await DownloadPageAsync();
            if (MyVM.entry != null)
            {
                AddrBook.ItemsSource = MyVM.entry;
            }
        }
        private async Task DownloadPageAsync()
        {

            string page = "https://www.facebook.com/feeds/notes.php?id=169952729703512&viewer=100000103724778&key=AWg34koXay1j5ZC9&format=json";
            RootObject feed = new RootObject();
            var client = new HttpClient();
            var data = await client.GetStringAsync(page);
            string result = data.ToString();
            

            feed = JsonConvert.DeserializeObject<RootObject>(result);
            foreach (var item in feed.entries)
            {
                MyVM.entry.Add(item);
            }
            //MyVM.entry = feed.entries;
        }
        private void MainLongListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AddrBook.SelectedItem == null)
            {
                return;
            }
            NavigationService.Navigate(new Uri("/Detail.xaml?detail=" + Htmlrmover.StripSpaceCharArray((AddrBook.SelectedItem as Entry).content.__html) , UriKind.Relative));
        }

         
        private void add_Click(object sender, RoutedEventArgs e)
        {
            Entry newEntry = new Entry();
            newEntry.title = enterTitle.Text;
            newEntry.summary = enterSummery.Text;
            //newEntry.content.__html = "just test";
            MyVM.entry.Add(newEntry);
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}