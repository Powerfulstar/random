﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace Collected_Notes_and_Discussion.Model
{
     
    public static class Htmlrmover
    {
         
        public static string StripTagsCharArray(string source)
        {
	        char[] array = new char[source.Length];
	        int arrayIndex = 0;
	        bool inside = false;

	        for (int i = 0; i < source.Length; i++)
	        {
	            char let = source[i];
	            if (let == '<')
	            {
		            inside = true;
		            continue;
	            }
	            if (let == '>')
	            {
		            inside = false;
		            continue;
	            }
                
	            if (!inside)
	            {
		            array[arrayIndex] = let;
		            arrayIndex++;
	            }
	        }
	        return new string(array, 0, arrayIndex);
        }
        public static string StripSpaceCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '&')
                {
                    inside = true;
                    continue;
                }
                if (let == ';')
                {
                    inside = false;
                    continue;
                }

                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
    }
}
