﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collected_Notes_and_Discussion.Model
{
    public class Author
    {
        public string name { get; set; }
        public string uri { get; set; }
    }

    public class Author2
    {
        public string name { get; set; }
        public string uri { get; set; }
    }

    public class Image
    {
        public string title { get; set; }
        public string url { get; set; }
        public string link { get; set; }
    }

    public class Content : INotifyPropertyChanged
    {
        private string html;
        public string __html
        {
            get
            {
                return this.html;
            }
            set
            {
                if (this.html != value)
                {
                    this.html = value;
                    this.NotifyPropertyChanged("__html");
                }
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }

    public class Entry : INotifyPropertyChanged
    {
        private string _title;
        public string title
        {
            get
            {
                return this._title;
            }
            set
            {
                if (this._title != value)
                {
                    this._title = value;
                    this.NotifyPropertyChanged("title");
                }
            }
        }
        public string id { get; set; }
        public string alternate { get; set; }
        public object categories { get; set; }

        private string _published;
        public string publishedget
        {
            get
            {
                return this._published;
            }
            set
            {
                if (this._published != value)
                {
                    this._published = value;
                    this.NotifyPropertyChanged("publishedget");
                }
            }
        }
        public string updated { get; set; }
        public Author2 author { get; set; }
        public string summary { get; set; }
        public Image image { get; set; }
        public string verb { get; set; }
        public string target { get; set; }
        public string objects { get; set; }
        public string comments { get; set; }
        private Content _content;
        public string likes { get; set; }
        public Content content
        {
            get
            {
                return this._content;
            }
            set
            {
                if (this._content != value)
                {
                    this._content = value;
                    this.NotifyPropertyChanged("content");
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }

    public class RootObject 
    {
        public string title { get; set; }
        public string link { get; set; }
        public string self { get; set; }
        public string updated { get; set; }
        public string icon { get; set; }
        public Author author { get; set; }
        public ObservableCollection<Entry> entries { get; set; }

        
    }
    public class Show
    {
        public string Summary { get; set; }
        public string Title { get; set; }        
    }
}
