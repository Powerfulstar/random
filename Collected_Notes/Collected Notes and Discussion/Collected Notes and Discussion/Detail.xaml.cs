﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Collected_Notes_and_Discussion.Model;

namespace Collected_Notes_and_Discussion
{
    public partial class Detail : PhoneApplicationPage
    {
        public Detail()
        {
            InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string detail = "";
            string main = "";
            if (NavigationContext.QueryString.TryGetValue("detail", out detail))
            {
                main = Htmlrmover.StripTagsCharArray(detail);
                //main = Htmlrmover.StripSpaceCharArray(main);
                //_content.Text = main;
                browser.NavigateToString("<html><head><meta charset="+"utf-8"+"/><body>"+detail+"</body></html>");
            }
        }
    }
}