using GalaSoft.MvvmLight;
using Collected_Notes_and_Discussion.Model;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
namespace Collected_Notes_and_Discussion.ViewModel
{
     
    public class MainViewModel : ViewModelBase
    {
        
        public RootObject feed { get; set; }
        public ObservableCollection<Entry> entry { get; set; }
        public string result = "";
        public MainViewModel()
        {            
            LoadData();                                   
        }
        public async void LoadData()
        {
            //StreamReader myFile = new StreamReader(@"Model\json.txt");
            //string myString = myFile.ReadToEnd();
            //feed = JsonConvert.DeserializeObject<RootObject>(myString);
            //entry = feed.entries;


            entry = (await DownloadPageAsync()).entries;
             
        }

        private async Task<RootObject> DownloadPageAsync()
        {

            string page = "https://www.facebook.com/feeds/notes.php?id=169952729703512&viewer=100000103724778&key=AWg34koXay1j5ZC9&format=json";

            var client = new HttpClient();
            var data = await client.GetStringAsync(page);
            result = data.ToString();


            feed = JsonConvert.DeserializeObject<RootObject>(result);
            return feed;
        }
        //public void LoadData()
        //{
        //    //StreamReader myFile = new StreamReader(@"Model\json.txt");
        //    //string myString = myFile.ReadToEnd();
        //    Task<string> resultob =  DownloadPageAsync();
        //    string myString = resultob.Result;
        //    feed = JsonConvert.DeserializeObject<RootObject>(myString);
        //    entry = feed.entries;
        //    //Task<RootObject> feedOB =  DownloadPageAsync();
        //    //entry = feedOB.Result.entries;        
              
        //}
        //private async Task<string> DownloadPageAsync()
        //{

        //    string page = "https://www.facebook.com/feeds/notes.php?id=169952729703512&viewer=100000103724778&key=AWg34koXay1j5ZC9&format=json";

        //    var client = new HttpClient();
        //    var data = await client.GetStringAsync(page);
        //    string resultinternal = data.ToString();

        //    return resultinternal;
        //}
    }
}