using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using System.IO;
using Where_s_The_Problem.Model;
using Where_s_The_Problem.Resources;

namespace Where_s_The_Problem.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainObject MyProperty { get; set; }
        public MainViewModel()
        {
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
            System.IO.StreamReader myFile = new System.IO.StreamReader(@"Resources\Data.txt");
            string myString = myFile.ReadToEnd();
            MyProperty = JsonConvert.DeserializeObject<MainObject>(myString);
            
        }
    }
}