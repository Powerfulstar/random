﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Where_s_The_Problem.Model
{
    public class From
    {
        public string category { get; set; }
        public string name { get; set; }
        public string id { get; set; }
    }

    public class Privacy
    {
        public string value { get; set; }
    }

    public class Location
    {
        public string street { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string zip { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
    }

    public class Place
    {
        public string id { get; set; }
        public string name { get; set; }
        public Location location { get; set; }
    }

    public class __invalid_type__0
    {
        public string id { get; set; }
        public string name { get; set; }
        public int offset { get; set; }
        public int length { get; set; }
        public string type { get; set; }
    }

    public class StoryTags
    {
        public List<__invalid_type__0> __invalid_name__0 { get; set; }
    }

    public class Application
    {
        public string name { get; set; }
        public string id { get; set; }
    }

    public class Datum
    {
        public string id { get; set; }
        public From from { get; set; }
        public string message { get; set; }
        public Privacy privacy { get; set; }
        public Place place { get; set; }
        public string type { get; set; }
        public string status_type { get; set; }
        public string created_time { get; set; }
        public string updated_time { get; set; }
        public string story { get; set; }
        public StoryTags story_tags { get; set; }
        public Application application { get; set; }
    }

    public class Paging
    {
        public string previous { get; set; }
        public string next { get; set; }
    }

    public class MainObject
    {
        public List<Datum> data { get; set; }
        public Paging paging { get; set; }
    }
}
