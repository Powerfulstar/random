﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using LocationFinder.Resources;
using System.Device.Location;
using System.Collections.ObjectModel;
using GART.Data;
using GART.BaseControls;

namespace LocationFinder
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        private readonly GeoCoordinateWatcher _GeoWatcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
        private ObservableCollection<ARItem> locations;
        private GeoPosition<GeoCoordinate> CurrentPosition { get; set; }

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            _GeoWatcher.PositionChanged += (o, args) => Dispatcher.BeginInvoke(() =>
            {
                CurrentPosition = _GeoWatcher.Position;

                locations = new ObservableCollection<ARItem>
                {
                    new Destination()
                    {
                        GeoLocation = new GeoCoordinate(23.786169,90.415613), 
                        Content = "Enosis BD Office Gulshan",
                        Description = GetLocationText(23.776345, 90.41609)
                    },
                    new Destination()
                    {
                        GeoLocation = new GeoCoordinate(23.777162,90.416112), 
                        Content = "Microsoft Bangladesh Office",
                        Description = GetLocationText(23.774594, 90.41200299999998)
                    },
                    new Destination()
                    {
                        GeoLocation = new GeoCoordinate(23.728882,90.415825), 
                        Content = "Leads Corportion Ltd",
                        Description = GetLocationText(23.779672, 90.41493)
                    },
                    new Destination()
                    {
                        GeoLocation = new GeoCoordinate(23.764275,90.406662),
                        Content = "Ahsanullah University Of Science & Technology",
                        Description = GetLocationText(23.764275,90.406662)
                    },
                    new Destination()
                    {
                        GeoLocation = new GeoCoordinate(23.887031050398946,90.405711103230715),
                        Content = "Home Sweet Home ^_^",
                        Description = GetLocationText(23.887031050398946,90.405711103230715)
                    }
                };

                ardisplay.ARItems = locations;
            });

            _GeoWatcher.Start();

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private string GetLocationText(double lat, double lon)
        {
            if (CurrentPosition != null && CurrentPosition.Location != null)
            {
                var start = new GeoCoordinate(CurrentPosition.Location.Latitude, CurrentPosition.Location.Longitude);
                var end = new GeoCoordinate(lat, lon);
                var distance = start.GetDistanceTo(end);

                return distance < 1000
                    ? string.Format("{0}m away.", Math.Round((double)distance, 0))
                    : string.Format("{0}km away.", Math.Round((double)distance / 1000, 2));
            }

            return string.Empty;
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            // Stop AR services
            ardisplay.StopServices();

            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            // Start AR services
            ardisplay.StartServices();

            base.OnNavigatedTo(e);
        }

        protected override void OnOrientationChanged(OrientationChangedEventArgs e)
        {
            base.OnOrientationChanged(e);

            if (ardisplay != null)
            {
                var orientation = ControlOrientation.Default;

                switch (e.Orientation)
                {
                    case PageOrientation.LandscapeLeft:
                        orientation = ControlOrientation.Clockwise270Degrees;
                        ardisplay.Visibility = Visibility.Visible;
                        break;
                    case PageOrientation.LandscapeRight:
                        orientation = ControlOrientation.Clockwise90Degrees;
                        ardisplay.Visibility = Visibility.Visible;
                        break;

                }

                ardisplay.Orientation = orientation;
            }
        }

        private void StackPanel_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

            string co = locations[0].GeoLocation.ToString();
            NavigationService.Navigate(new Uri("/Map.xaml?coordinate=" + co, UriKind.Relative));
        }

    }
}