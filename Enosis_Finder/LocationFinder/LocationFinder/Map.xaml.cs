﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Device.Location;
using Microsoft.Phone.Maps.Controls;
using Windows.Devices.Geolocation;
using Microsoft.Phone.Maps.Toolkit;
using System.Windows.Media;
using Microsoft.Phone.Maps.Services;

namespace LocationFinder
{
    public partial class Map : PhoneApplicationPage
    {
        public Map()
        {

            InitializeComponent();
            ShowMyLocationOnTheMap();


        }
        public GeoCoordinate myGeoCoordinate;
        public GeoCoordinate destination;
        MapRoute calculatedMapRoute;
        public string coordinate = "";
        private async void ShowMyLocationOnTheMap()
        {
            // Get my current location.
            Geolocator myGeolocator = new Geolocator();
            Geoposition myGeoposition = await myGeolocator.GetGeopositionAsync();
            Geocoordinate myGeocoordinate = myGeoposition.Coordinate;
            myGeoCoordinate =
            CoordinateConverter.ConvertGeocoordinate(myGeocoordinate);


            // Make my current location the center of the Map.
            this.map.Center = myGeoCoordinate;
            this.map.ZoomLevel = 13;

            try
            {
                MapLayer layer = new MapLayer();
                MapOverlay overlayer = new MapOverlay();




                overlayer.GeoCoordinate = myGeoCoordinate;
                Pushpin myPosition = new Pushpin();
                map.SetView(myGeoCoordinate, 16);
                myPosition.Content = "My Location";
                myPosition.Foreground = new SolidColorBrush(Colors.Red);
                overlayer.Content = myPosition;

                layer.Add(overlayer);
                map.Focus();

                map.Layers.Add(layer);
            }
            catch (Exception)
            {
                MessageBox.Show("Couldnt Detect Your Location");
            }
        }
        List<string> parser(string parseIT)
        {
            List<string> parsed = new List<string>();
            bool flag = false;
            string parsed1 = "";
            string parsed2 = "";
            for (int i = 0; i < parseIT.Length; i++)
            {

                if (parseIT[i] != ',' && !flag)
                {
                    parsed1 += parseIT[i];
                }
                else if (flag)
                {
                    parsed2 += parseIT[i];
                }
                else if (parseIT[i] == ',')
                {
                    flag = true;
                }

            }
            parsed.Add(parsed1.Trim());
            parsed.Add(parsed2.Trim());
            return parsed;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (NavigationContext.QueryString.TryGetValue("coordinate", out coordinate))
            {
                try
                {
                    MapLayer layer = new MapLayer();
                    MapOverlay overlayer = new MapOverlay();

                    List<string> temp = new List<string>();
                    temp = parser(coordinate);

                    destination = new System.Device.Location.GeoCoordinate((Convert.ToDouble(temp[0])), Convert.ToDouble(temp[1]));
                    overlayer.GeoCoordinate = destination;
                    Pushpin myPosition = new Pushpin();
                    map.SetView(new System.Device.Location.GeoCoordinate((Convert.ToDouble(temp[0])), Convert.ToDouble(temp[1])), 16);
                    myPosition.Content = "Enosis BD";
                    myPosition.Foreground = new SolidColorBrush(Colors.Red);
                    overlayer.Content = myPosition;

                    layer.Add(overlayer);
                    map.Focus();

                    map.Layers.Add(layer);
                }
                catch (Exception meh)
                {

                    MessageBox.Show(meh.Message);
                }
            }
        }


        private void routeQuery_QueryCompleted(object sender, QueryCompletedEventArgs<Route> e)
        {
            Route theRoute = e.Result;
            calculatedMapRoute = new MapRoute(theRoute);
            map.AddRoute(calculatedMapRoute);

        }

        private void route_Click(object sender, RoutedEventArgs e)
        {

            if (myGeoCoordinate == null)
            {
                MessageBox.Show("Your Location is not detected");
            }
            else
            {
                if (calculatedMapRoute != null)
                {
                    map.RemoveRoute(calculatedMapRoute);
                }



                RouteQuery routeQuery = new RouteQuery();
                routeQuery.QueryCompleted += routeQuery_QueryCompleted;


                if (!routeQuery.IsBusy)
                {
                    List<GeoCoordinate> routeCoordinates = new List<GeoCoordinate>();
                    routeCoordinates.Add(new GeoCoordinate(myGeoCoordinate.Latitude, myGeoCoordinate.Longitude)); //Eiffel Tower coordinates
                    routeCoordinates.Add(new GeoCoordinate(destination.Latitude, destination.Longitude));

                    routeQuery.Waypoints = routeCoordinates;
                    routeQuery.QueryAsync();

                    map.Center = new GeoCoordinate(destination.Latitude, destination.Longitude); //Center map on first coordinates
                }

                else
                {
                    // user clicked no
                }
            }
        }
    }
}