﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System.Net;
using System.IO;

namespace TrainFairPerser
{
    class Program
    {
        static void Main(string[] args)
        {


            string url = @"E:\Train Fair\Ticket fare for East Zone.htm";
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);
            List<TrainFairObject> list = new List<TrainFairObject>();

            //// /html/body/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td[1]
            //*[@id="miInfo"]/tbody/tr[1]/td[1]
            for (int i = 5; i < 386; i++)
            {
                HtmlNodeCollection metaTags = doc.DocumentNode.SelectNodes(@"//tbody/tr/td[2]/table/tbody/tr["+i+"]/td");
                Console.WriteLine(metaTags);
                TrainFairObject ob = new TrainFairObject();
                if (metaTags != null)
                {
                    
                    foreach (var item in metaTags)
                    {
                        if (ob.From == null)
                        {
                            ob.From = item.InnerText;
                        }
                        else if (ob.To == null)
                        {
                            ob.To = item.InnerText;
                        }
                        else if (ob.SeconCLassFair == null)
                        {
                            ob.SeconCLassFair = item.InnerText;
                        }
                        else if (ob.SecondCLassMail == null)
                        {
                            ob.SecondCLassMail = item.InnerText;
                        }
                        else if (ob.Commuter == null)
                        {
                            ob.Commuter = item.InnerText;
                        }
                        else if (ob.Sulov == null)
                        {
                            ob.Sulov = item.InnerText;
                        }
                        else if (ob.Shovon == null)
                        {
                            ob.Shovon = item.InnerText;
                        }
                        else if (ob.ShovonChair == null)
                        {
                            ob.ShovonChair = item.InnerText;
                        }
                        else if (ob.FirstClassChairSeat == null)
                        {
                            ob.FirstClassChairSeat = item.InnerText;
                        }
                        else if (ob.FirstClassBirth == null)
                        {
                            ob.FirstClassBirth = item.InnerText;
                        }
                        else if (ob.Snigdha == null)
                        {
                            ob.Snigdha = item.InnerText;
                        }
                        else if (ob.ACSeat == null)
                        {
                            ob.ACSeat = item.InnerText;
                        }
                        else if (ob.ACBirth == null)
                        {
                            ob.ACBirth = item.InnerText;
                        }
                        File.AppendAllText(@"E:\Train Fair\xpath.txt", item.InnerText + "\n");
                    }

                }
                File.AppendAllText(@"E:\Train Fair\xpath.txt", "\n\n\n\n\n");
                list.Add(ob);
            }

            File.WriteAllText(@"E:\Train Fair\json.txt", JsonConvert.SerializeObject(list));

            

             
            
            //Console.ReadLine();
        }
    }
}
