﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainFairPerser
{
    class TrainFairObject
    {
        public string From { get; set; }
        public string To { get; set; }
        public string SeconCLassFair { get; set; }
        public string SecondCLassMail { get; set; }
        public string Commuter { get; set; }
        public string Sulov { get; set; }
        public string Shovon { get; set; }
        public string ShovonChair { get; set; }
        public string FirstClassChairSeat { get; set; }
        public String FirstClassBirth { get; set; }
        public string Snigdha { get; set; }
        public string ACSeat { get; set; }
        public string ACBirth { get; set; }
    }
    class Translate
    {
        public string Bangla { get; set; }
        public string English { get; set; }
    }
}
