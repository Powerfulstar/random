﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainFairPerser;
using Newtonsoft.Json;
using System.IO;
namespace TrainJsonFormatter
{
    class Program
    {
        static void Main(string[] args)
        {
            List<TrainFairObject> fair = new List<TrainFairObject>();
            fair = JsonConvert.DeserializeObject<List<TrainFairObject>>(File.ReadAllText(@"E:\Train Fair\json.txt"));
            List<Translate> translate = new List<Translate>();
            foreach (var item in fair)
            {
                Translate tr = new Translate();
                tr.Bangla = item.From;                
                translate.Add(tr);
                tr.Bangla = item.To;
                translate.Add(tr);
            }

            List<Translate> translate2 = new List<Translate>();
             
            foreach (var item in translate.Select(x => x.Bangla).Distinct())
            {
                for (int i = 0; i < translate.Count; i++)
                {
                    if (translate[i].Bangla == item)
                    {
                        translate2.Add(translate[i]);
                        
                        break;
                    }
                }
            }

            string result = JsonConvert.SerializeObject(translate2);
            File.WriteAllText(@"E:\Train Fair\jsonTranslate.txt",result);
        }
    }
}
